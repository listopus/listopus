
function autoco(f, i) {
  var field = document.getElementById(f);
  if (field.value == '') {
    document.getElementById(i).style.display = "none";
  } else{
    document.getElementById(i).style.display = "block";
  }
}
window.onclick = function(event) {
  if (!event.target.matches('.sfi')) {
    const boxes = document.getElementsByClassName('dropdown-content');
    for (const box of boxes) {
      box.style.display = 'none';
    }
  }
}
function acset(val, i){
  document.getElementById(i).value = val;
  document.getElementById("sform").submit();
}
      
function srunsave(link, cat, url) {
  var element = document.getElementById("save-row-" + link);
  var element2 = document.getElementById("unsave-row-" + link);
  const xhttp = new XMLHttpRequest();
  xhttp.open("GET", "/unsave?link=" + link + "&cat=" + cat + "&url=" + url);
  xhttp.send();
  setTimeout(function () {
      element2.style.display = "none";
      element.style.display = "inline-block";
  }, 250);
}

function srreport(link, cat, url) {
  var element = document.getElementById("report-row-" + link);
  var element2 = document.getElementById("unreport-row-" + link);
  const xhttp = new XMLHttpRequest();
  xhttp.open("GET", "/report?link=" + link + "&cat=" + cat + "&url=" + url);
  xhttp.send();
  setTimeout(function () {
      element.style.display = "none";
      element2.style.display = "inline-block";
  }, 250);
}
function srunreport(link, cat, url) {
  var element = document.getElementById("report-row-" + link);
  var element2 = document.getElementById("unreport-row-" + link);
  const xhttp = new XMLHttpRequest();
  xhttp.open("GET", "/unreport?link=" + link + "&cat=" + cat + "&url=" + url);
  xhttp.send();
  setTimeout(function () {
      element.style.display = "inline-block";
      element2.style.display = "none";
  }, 250);
}
  
function setimg(core, more) {
  var element = document.getElementById("fullimgx");
  element.src = core;
}
function loadimages() {     
    const boxes = document.getElementsByClassName('res-img');
    for (const box of boxes) {
      link = box.getAttribute("lazy");
      box.src = link
    }
  }

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
  }