require File.expand_path('../boot', __FILE__)

require "action_controller/railtie"
require "action_view/railtie"
#require "sprockets/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Listopus
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :'en'
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml,yaml}')]
    config.i18n.fallbacks = true 
    config.i18n.fallbacks = %i[en]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    #config.active_record.raise_in_transactional_callbacks = true
    config.action_dispatch.perform_deep_munge = false
    
    #config.assets.enabled = false
    #config.assets.initialize_on_precompile = false
	
	config.middleware.use Rack::Deflater
	@options = {
			 :enabled => true,
			 :remove_multi_spaces => true,
			 :remove_comments => true,
			 :remove_intertag_spaces => true,
			 :remove_quotes => false,
			 #:compress_css => true,
			 #:css_compressor => :yui,
			 #:compress_javascript => true,
			 #:javascript_compressor => :yui,
			 :simple_doctype => true,
			 :remove_script_attributes => false,
			 :remove_style_attributes => false,
			 :remove_link_attributes => false,
			 :remove_form_attributes => false,
			 :remove_input_attributes => false,
			 :remove_javascript_protocol => false,
			 :remove_http_protocol => false,
			 :remove_https_protocol => false,
			 :preserve_line_breaks => false,
			 :simple_boolean_attributes => false,
			 :compress_js_templates => false
		 }
	config.middleware.use HtmlCompressor::Rack, @options
  end
end
