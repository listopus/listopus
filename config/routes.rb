Rails.application.routes.draw do
  root 'welcome#index'
  get 'search' => 'welcome#search', as: 'search'
  get 'save' => 'welcome#save', as: 'save'
  get 'unsave' => 'welcome#unsave', as: 'unsave'
  get 'report' => 'welcome#report', as: 'report'
  get 'unreport' => 'welcome#unreport', as: 'unreport'
  get 'saved' => 'welcome#saved', as: 'saved'
  get 'privacy' => 'welcome#privacy', as: 'privacy'
  get 'contact' => 'welcome#contact', as: 'contact'
  get 'auto' => 'welcome#auto', as: 'auto'
end
