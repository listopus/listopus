class WelcomeController < ApplicationController
  before_action :set_sarray, only: ["index", "search"]
  
  def index
  end
  
  def search
    require "elasticsearch"
    require 'json'

    searchclient = Elasticsearch::Client.new(
      url: "https://2x4tp7rec4:p6s6rovw46@list-6840506533.us-east-1.bonsaisearch.net:443",
      retry_on_failure: 5,
      request_timeout: 30,
      log: false
    )

    if params[:q].present?
    query = {}
    query[:bool] = {}
    query[:bool][:must] = []
    query[:bool][:must] << {simple_query_string: {query: params[:q].to_s, fields: ["a^10", "b^5", "c"], default_operator: "and"}}
    else
    query = {}
    query[:bool] = {}
    query[:bool][:must] = []
    query[:bool][:must] << {match_all: {}}
    end

    if ((params[:minp].present?) && (params[:maxp].present?))
      query[:bool][:must] << {range: {m: {gte: params[:minp].to_i, lte: params[:maxp].to_i }}}
    elsif ((params[:minp].present?) && (!params[:maxp].present?))
      query[:bool][:must] << {range: {m: {gte: params[:minp].to_i }}}
    elsif ((!params[:minp].present?) && (params[:maxp].present?))
      query[:bool][:must] << {range: {m: {lte: params[:maxp].to_i }}}
    end
    
    sortarray = ["_score"]
    sortarray << {m: "asc"} if (params[:sort].to_s == "masc")
    sortarray << {m: "desc"} if (params[:sort].to_s == "mdesc")
    sortarray << {y: "desc"} if (params[:sort].to_s == "ydesc")
    sortarray << {e: "desc"} if (params[:sort].to_s == "edesc")
    sort = sortarray.reverse

    from = (params[:page].to_i - 1) * 20
    size = 20
    
    results = searchclient.search(
      index: "p", 
      body: {
        query: query,
        sort: sort,
        from: from,
        size: size 
      }
    )
    
    @total_results = results.to_h['hits']['total']['value']
    @results = results.to_h['hits']['hits']

    # Did you mean: ...?
suggestion = searchclient.search(
  index: "auton",
  body: { 
    "suggest": {
    "text": params[:q].to_s,
    "simple_phrase": {
      "phrase": {
        "field": "title.trigram",
        "size": 1,
        "gram_size": 3,
        "direct_generator": [ {
          "field": "title.trigram",
          "suggest_mode": "always"
        } ],
        "highlight": {
          "pre_tag": "<strong><em>",
          "post_tag": "</em></strong>"
        }
      }
    }
    }
  }
)
@total_suggests = suggestion.to_h['suggest']['simple_phrase'][0]['options']
    
    if (params[:page].present?) && (params[:page].to_i > 25)
      redirect_to root_path
    end

    # render RSS
    if params[:rss].to_s == "true"
  		render "welcome/rss", :layout => false, content_type: "application/xml"
  	end

    unless cookies[:hsh].present?
    # queries cookies
    if params[:q].present?
    cookies.permanent["q#{params[:c]}"] = JSON.generate([]) if cookies["q#{params[:c]}"].blank?
    array = JSON.parse(cookies["q#{params[:c]}"])
    new_array = array << params[:q].to_s.downcase
    new_array = new_array.uniq.last(10)
    cookies.permanent["q#{params[:c]}"] = JSON.generate(new_array)
    end
    end

    @startol = from + 1
    
  end
  
  def save
    #update ES record
    require "elasticsearch"
    require 'json'

    searchclient = Elasticsearch::Client.new(
      url: "https://2x4tp7rec4:p6s6rovw46@list-6840506533.us-east-1.bonsaisearch.net:443",
      retry_on_failure: 5,
      request_timeout: 30,
      log: false
    )
    cookies.permanent["#{params[:cat]}"] = JSON.generate([]) if cookies["#{params[:cat]}"].blank?
    link = params[:link].to_s
    if (cookies["#{params[:cat]}"].present?) && (JSON.parse(cookies["#{params[:cat]}"]).exclude? link)
      array = JSON.parse(cookies["#{params[:cat]}"])
      new_array = array << link
      if (cookies["#{params[:cat]}"].size.to_i > 3800)
        new_array = new_array.drop(1)
      end
      cookies.permanent["#{params[:cat]}"] = JSON.generate(new_array)
      current = searchclient.get(id: params[:link].to_s, index: "#{params[:cat]}")["_source"]["y"]
      count = current.to_i + 1
      cookies.permanent["stop#{params[:cat]}"] = "true"
      searchclient.update(id: params[:link].to_s, index: "#{params[:cat]}", body: { doc: {"y": count} })
    end
  end
  
  def unsave
    #update ES record
    require "elasticsearch"
    require 'json'

    searchclient = Elasticsearch::Client.new(
      url: "https://2x4tp7rec4:p6s6rovw46@list-6840506533.us-east-1.bonsaisearch.net:443",
      retry_on_failure: 5,
      request_timeout: 30,
      log: false
    )
    link = params[:link].to_s
    if (cookies["#{params[:cat]}"].present?) && (JSON.parse(cookies["#{params[:cat]}"]).include? link)
      array = JSON.parse(cookies["#{params[:cat]}"])
      new_array = array - [link]
      cookies.permanent["#{params[:cat]}"] = JSON.generate(new_array)
      cookies.delete("#{params[:cat]}") if (new_array.count == 0)
      current = searchclient.get(id: params[:link].to_s, index: "#{params[:cat]}")["_source"]["y"]
      count = current.to_i - 1
      searchclient.update(id: params[:link].to_s, index: "#{params[:cat]}", body: { doc: {"y": count} })
    end
  end
  
  def report
    #update ES record
    require "elasticsearch"
    require 'json'

    searchclient = Elasticsearch::Client.new(
      url: "https://2x4tp7rec4:p6s6rovw46@list-6840506533.us-east-1.bonsaisearch.net:443",
      retry_on_failure: 5,
      request_timeout: 30,
      log: false
    )
    cookies.permanent[:report] = JSON.generate([]) if cookies[:report].blank?
    link = params[:link].to_s
    if (cookies[:report].present?) && (JSON.parse(cookies[:report]).exclude? link)
      array = JSON.parse(cookies[:report])
      new_array = array << link
      if (cookies[:report].size.to_i > 3800)
        new_array = new_array.drop(1)
      end
      cookies.permanent[:report] = JSON.generate(new_array)
      current = searchclient.get(id: params[:link].to_s, index: "#{params[:cat]}")["_source"]["z"]
      count = current.to_i + 1
      searchclient.update(id: params[:link].to_s, index: "#{params[:cat]}", body: { doc: {"z": count} })
    end

    puts params
    redirect_back(fallback_location: root_path)
  end
  
  def saved
    require "elasticsearch"
    require 'json'
    require 'httparty'

    if cookies["p"].present?
    searchclient = Elasticsearch::Client.new(
      url: "https://2x4tp7rec4:p6s6rovw46@list-6840506533.us-east-1.bonsaisearch.net:443",
      retry_on_failure: 5,
      request_timeout: 30,
      log: false
    ) 

    results = searchclient.mget(
      index: "p", 
      body: { 
          ids: JSON.parse(cookies["p"])
      }
    )
    
    @total_results_p = results.to_h['docs'].count
    @results_p = results.to_h['docs'].reverse
    else
    @total_results_p = 0
    end

    @all_count = @total_results_p
  end
  
  def privacy
  end

  def auto
    if cookies[:hss].present?
      array = []
    else
    # limit string to 40 characters
    require "elasticsearch"
    require 'json'

    searchclient = Elasticsearch::Client.new(
      url: "https://2x4tp7rec4:p6s6rovw46@list-6840506533.us-east-1.bonsaisearch.net:443",
      retry_on_failure: 5,
      request_timeout: 30,
      log: false
    )

 results = searchclient.search(
  index: "auton",
  body: { 
    query: { 
      "multi_match": {
      "query": params[:q].to_s,
      "type": "bool_prefix",
      "fields": [
        "terms",
        "terms._2gram",
        "terms._3gram"
      ]
      }
    }, 
  "collapse": {
    "field": "title"         
  },
    "suggest": {
    "text": params[:q].to_s,
    "simple_phrase": {
      "phrase": {
        "field": "title.trigram",
        "size": 1,
        "gram_size": 3,
        "direct_generator": [ {
          "field": "title.trigram",
          "suggest_mode": "always"
        } ]
      }
    }
    },
    size: 8
  }
)
    total_results = results.to_h['hits']['total']['value']
    if total_results.to_i > 0
    allresults = results.to_h['hits']['hits']
    array = allresults.map { |x| x['_source']['terms'] }
    else
      total_suggests = results.to_h['suggest']['simple_phrase'][0]['options']
      if total_suggests.count > 0
      query = total_suggests[0]['text']
results = searchclient.search(
  index: "auton",
  body: { 
    query: { 
      "multi_match": {
      "query": query,
      "type": "bool_prefix",
      "fields": [
        "terms",
        "terms._2gram",
        "terms._3gram"
      ]
      }
    }, 
  "collapse": {
    "field": "title"         
  },
    size: 8
  }
)
    total_results = results.to_h['hits']['total']['value']
    if total_results.to_i > 0
    allresults = results.to_h['hits']['hits']
    array = allresults.map { |x| x['_source']['terms'] }
    else
      array = []
    end
      else
        array = []
      end
    end
    end # end of cookie condition
    render json: array.first(8)
   end

  def set_sarray
    if cookies[:hsh].present?
      @sarray = []
    else
    if ((params[:c].present?) && (["s","r", "j"].include? "#{params[:c]}"))
     if (cookies["q#{params[:c]}"].present?)
      @sarray = JSON.parse(cookies["q#{params[:c]}"]).reverse
     else 
      @sarray = []
     end
    else
     if (cookies["qp"].present?)
      @sarray = JSON.parse(cookies["qp"]).reverse
     else 
      @sarray = []
     end
    end
    end # end of cookie condition

    @psarray = ["fresh eggs", "used cars"]
    
  end

  end