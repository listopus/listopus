class ApplicationController < ActionController::Base
  before_action :set_cookies

  def set_cookies
    if cookies[:countrycode].blank?
      db = MaxMindDB.new(Rails.root.join('public', 'GeoLite2-Country.mmdb'))
      ret = db.lookup(request.remote_ip)

      ip_country_code = if ret.found?
                          ret.country.iso_code.downcase
                        else
                          'us'
                        end

      cookies.permanent[:countrycode] = ip_country_code.to_s
    end

    languages = %w[en es fr pt de it zh zh-tw ar nl ru ja ko tr sv fi nb da cs pl sk et lv lt el ro bg hu sl uk id ms vi th he tl hi sr hr bs sq mk]
    larray = request.env['HTTP_ACCEPT_LANGUAGE'].split(",").map {|x| x.split(";").first}.map {|x| x.split("-").first}.flatten
    inter = larray & languages
    http = inter.first
    if inter.count > 0
      if %w[no nb nn].include? http.to_s
        I18n.locale = 'nb'
      else
        I18n.locale = http.to_s
      end
    else
      I18n.locale = "en"
    end

    # set country based on cookie
    @con = if cookies[:countrycode].present?
             cookies[:countrycode].to_s
           else
             'us'
           end

    if cookies[:currency_code].present?
      @cur = cookies[:currency_code].to_s
    else
      body = JSON.parse(File.read("#{Rails.root}/lib/data/currencies.json"))
      @cur = body[@con.upcase] ? (body[@con.upcase]) : 'USD'
      cookies.permanent[:currency_code] = @cur
    end

    cookies.permanent[:cp] = "true" unless cookies[:stopp].present?
    # uid
    cookies.permanent[:t] = Time.now.to_i.to_s unless cookies[:t]
  end
end
