module ApplicationHelper

  def pagination_numbers(current, size, results)
    array = []
    
    page = current.to_i
    last = ([results.to_i, 500].min/size.to_f).ceil
    if page <= 5
     if last > 5
    array = [1, 2, 3, 4, 5, "...", ">"]
     else
      if last > 1
    tarray = (1..last).to_a
    parray = []
    narray = []
       if page != tarray.last
      narray = [">"]
       end
       if page != tarray.first
      parray = ["<"]
       end
    array = parray + tarray + narray  
     else
      array = []
     end
    end # last > 5
    elsif page > (last - 3)
      array = ["<", "...", last - 4, last - 3, last - 2, last - 1, last]
    else
    prevtwo = page - 2
    prevone = page - 1
    nextone = page + 1
    nexttwo = page + 2
    array = ["<", "...", prevtwo, prevone, page, nextone, nexttwo, "...", ">"]
    end
    array = array - [0]
  end

  def full_pages(size, results)
    last = ([results.to_i, 500].min/size.to_f).ceil
    (1..last).to_a
  end
  
  def is_rtl_language(loc)
      ["ar", "he"].include? loc.to_s
  end
  
  def ago_after(loc)
      ["en", "it", "pt", "nl", "sv", "fi", "nb", "da", "pl", "et", "hu", "uk", "ru", "id", "sq", "tr", "ms", "tl", "vi", "ja", "zh", "zh-tw", "ko", "th", "he", "hi"].include? loc.to_s
  end
  
  def language_array(locale)
    codes = ["ab", "aa", "af", "ak", "sq", "am", "ar", "an", "hy", "as", "av", "ae", "ay", "az", "bm", "ba", "eu", "be", "bn", "bi", "bs", "br", "bg", "my", "ca", "ch", "ce", "zh", "zh-TW", "cu", "cv", "kw", "co", "cr", "hr", "cs", "da", "dv", "nl", "dz", "eo", "et", "ee", "fo", "fj", "fi", "fr", "ff", "gl", "lg", "ka", "de", "el", "gn", "gu", "ht", "ha", "he", "hz", "hi", "ho", "hu", "is", "io", "ig", "id", "ia", "ie", "iu", "ik", "ga", "it", "ja", "jv", "kl", "kn", "kr", "ks", "kk", "km", "ki", "rw", "kv", "kg", "ko", "kj", "ku", "ky", "lo", "la", "lv", "li", "ln", "lt", "lu", "lb", "mk", "mg", "ms", "ml", "mt", "gv", "mi", "mr", "mh", "mn", "na", "nv", "ng", "ne", "nd", "se", "nb", "ny", "oc", "oj", "or", "om", "os", "pi", "ps", "fa", "pl", "pt", "pa", "qu", "ro", "rm", "rn", "ru", "sm", "sg", "sa", "sc", "gd", "sr", "sn", "ii", "sd", "si", "sk", "sl", "so", "nr", "st", "es", "su", "sw", "ss", "sv", "tl", "ty", "tg", "ta", "tt", "te", "th", "bo", "ti", "to", "ts", "tn", "tr", "tk", "tw", "uk", "ur", "ug", "uz", "ve", "vi", "vo", "wa", "cy", "fy", "wo", "xh", "yi", "yo", "za", "zu"]
    if locale.to_s == "en"
      codes = ["en"] + codes
      return codes
    else
      new_array = []
      codes.each do |code|
        new_array << [code.to_s, I18n.t("lang.#{code}", locale: locale.to_s).to_s]
      end
      new_array = new_array.sort_by{|ar| ar.second.downcase}
      new_array = new_array.map{|ar| ar.first}
      new_array = ["en"] + new_array
      return new_array
    end
  end
  
  def country_array(locale)
    codes = ["af", "ax", "al", "dz", "as", "ad", "ao", "ai", "aq", "ag", "ar", "am", "aw", "au", "at", "az", "bs", "bh", "bd", "bb", "by", "be", "bz", "bj", "bm", "bt", "bo", "ba", "bw", "br", "io", "vg", "bn", "bg", "bf", "bi", "kh", "cm", "ca", "cv", "bq", "ky", "cf", "td", "cl", "cn", "cx", "cc", "co", "km", "cg", "cd", "ck", "cr", "ci", "hr", "cu", "cw", "cy", "cz", "dk", "dj", "dm", "do", "ec", "eg", "sv", "gq", "er", "ee", "sz", "et", "fk", "fo", "fj", "fi", "fr", "gf", "pf", "tf", "ga", "gm", "ge", "de", "gh", "gi", "gr", "gl", "gd", "gp", "gu", "gt", "gg", "gw", "gn", "gy", "ht", "hn", "hk", "hu", "is", "in", "id", "ir", "iq", "ie", "im", "il", "it", "jm", "jp", "je", "jo", "kz", "ke", "ki", "xk", "kw", "kg", "la", "lv", "lb", "ls", "lr", "ly", "li", "lt", "lu", "mo", "mg", "mw", "my", "mv", "ml", "mt", "mh", "mq", "mr", "mu", "yt", "mx", "fm", "md", "mc", "mn", "me", "ms", "ma", "mz", "mm", "na", "nr", "np", "nl", "nc", "nz", "ni", "ne", "ng", "nu", "nf", "kp", "mk", "mp", "no", "om", "pk", "pw", "ps", "pa", "pg", "py", "pe", "ph", "pn", "pl", "pt", "pr", "qa", "re", "ro", "ru", "rw", "ws", "sm", "st", "sa", "sn", "rs", "sc", "sl", "sg", "sx", "sk", "si", "sb", "so", "za", "gs", "kr", "ss", "es", "lk", "bl", "sh", "kn", "lc", "mf", "pm", "vc", "sd", "sr", "sj", "se", "ch", "sy", "tw", "tj", "tz", "th", "tl", "tg", "tk", "to", "tt", "tn", "tr", "tm", "tc", "tv", "um", "vi", "ug", "ua", "ae", "gb", "uy", "uz", "vu", "va", "ve", "vn", "wf", "eh", "ye", "zm", "zw"]
    if locale.to_s == "en"
      codes = ["us"] + codes
      return codes
    else
      new_array = []
      codes = ["us"] + codes
      codes.each do |code|
        new_array << [code.to_s, I18n.t("country.#{code.upcase}", locale: locale.to_s).to_s]
      end
      new_array = new_array.sort_by{|ar| ar.second.downcase}
      new_array = new_array.map{|ar| ar.first}
      new_array = ["us"] + new_array
      return new_array
    end
  end
  
  def currencies_array(locale)
    codes = ["AFN", "ALL", "DZD", "AOA", "ARS", "AMD", "AWG", "AUD", "AZN", "BSD", "BDT", "BBD", "BZD", "BMD", "BOB", "BAM", "BWP", "BRL", "GBP", "BND", "BGN", "BIF", "KHR", "CAD", "CVE", "KYD", "XAF", "XPF", "CLP", "CNY", "COP", "KMF", "CDF", "CRC", "HRK", "CZK", "DKK", "DJF", "DOP", "XCD", "EGP", "ETB", "EUR", "FKP", "FJD", "GMD", "GEL", "GIP", "GTQ", "GNF", "GYD", "HTG", "HNL", "HKD", "HUF", "ISK", "INR", "IDR", "ILS", "JMD", "JPY", "KZT", "KES", "KGS", "LAK", "LBP", "LSL", "LRD", "MOP", "MKD", "MGA", "MWK", "MYR", "MVR", "MRO", "MUR", "MXN", "MDL", "MNT", "MAD", "MZN", "MMK", "NAD", "NPR", "ANG", "TWD", "NZD", "NIO", "NGN", "NOK", "PKR", "PAB", "PGK", "PYG", "PEN", "PHP", "PLN", "QAR", "RON", "RUB", "RWF", "WST", "SAR", "RSD", "SCR", "SLL", "SGD", "SBD", "SOS", "ZAR", "KRW", "LKR", "SHP", "SRD", "SZL", "SEK", "CHF", "STD", "TJS", "TZS", "THB", "TOP", "TTD", "TRY", "UGX", "UAH", "AED", "UYU", "UZS", "VUV", "VND", "XOF", "YER", "ZMW"]
    if locale.to_s == "en"
      codes = ["USD"] + codes
      return codes
    else
      new_array = []
      codes.each do |code|
        new_array << [code.to_s, I18n.t("currency.#{code.upcase}", locale: locale.to_s).to_s]
      end
      new_array = new_array.sort_by{|ar| ar.second.downcase}
      new_array = new_array.map{|ar| ar.first}
      new_array = ["USD"] + new_array
      return new_array
    end
  end
  
  def nonbold_language(language)
    ["zh", "zh-tw", "ja", "ar"].include? "#{language}"
  end
  
  def select_language
    ["en", "es", "fr", "pt", "de", "it", "zh", "zh-tw", "ar", "nl", "ru", "ja", "ko", "tr", "sv", "fi", "nb", "da", "cs", "pl", "sk", "et", "lv", "lt", "el", "ro", "bg", "hu", "sl", "uk", "id", "ms", "vi", "th", "he", "tl", "hi", "sr", "hr", "bs", "sq", "mk"]
  end

  def map_provider(cookie, location, latlon)
    
  if (cookie.to_s == "gm")                
    "https://www.google.com/maps/search/?api=1&query=" + location.to_s
  elsif (cookie.to_s == "bm")                
    "https://bing.com/maps/default.aspx?ss=" + location.to_s
  else
    "https://www.openstreetmap.org/search?query=" + location.to_s
  end

  end
    
end
